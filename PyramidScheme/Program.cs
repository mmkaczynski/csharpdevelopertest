﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PyramidScheme
{
    class Program
    {
        
        static void Main(string[] args)
        {
            XmlDocument pyramidDoc = new XmlDocument();
            if (File.Exists(@"piramida.xml"))
            {
                pyramidDoc.Load(@"piramida.xml");
            }
            XmlDocument transactionsDoc = new XmlDocument();
            if (File.Exists(@"przelewy.xml"))
            {
                transactionsDoc.Load(@"przelewy.xml");
            }
            PyramidSchemeController.Instance.Init(pyramidDoc,transactionsDoc);
            PyramidSchemeController.Instance.MakeTransactions();
            PyramidSchemeController.Instance.LogPyramid();
            //Console.ReadLine();
        }

    }
}
