﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyramidScheme
{
    class MemberModel
    {
        public string Id { get; set; }
        public MemberModel Superior { get; set; }
        public List<MemberModel> Subordinates { get; set; }
        public int Level { get; set; }

        public int SubordinatesWithoutSubordinates { get; set; } = 0;

        public int Money { get; set; } = 0;
    }
}
