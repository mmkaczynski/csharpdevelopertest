﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyramidScheme
{
    class TransactionModel
    {
        public string FromId { get; set; }
        public int Amount { get; set; } = 0;
    }
}
