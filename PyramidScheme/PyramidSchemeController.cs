﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PyramidScheme
{
    public class PyramidSchemeController
    {
        private Dictionary<string, MemberModel> _pyramid;
        private List<TransactionModel> _transactions;
        private bool _pyramidReady;
        private bool _transactionsReady;
        static bool _initialized;
        public static PyramidSchemeController Instance
        {
            get
            {
                
                if (!_initialized)
                {
                    _instance = new PyramidSchemeController();
                    _initialized = true;
                }

                return _instance;
            }
        }

        private static PyramidSchemeController _instance;


        public void Init(XmlDocument pyramidDoc, XmlDocument transactionsDoc)
        {
            if (SimpleValidatePyramidXml(pyramidDoc) && SimpleValidateTransactionsXml(transactionsDoc))
            {
                _pyramid = GeneratePyramidFromXml(pyramidDoc);
                _pyramidReady = true;
                _transactions = LoadTransactions(transactionsDoc);
                _transactionsReady = true;
            }
        }

        bool SimpleValidatePyramidXml(XmlDocument pyramidDoc)
        {
            bool isNameValid = pyramidDoc.DocumentElement.Name == "piramida";
            bool isCreatorValid = true;
            if (pyramidDoc.DocumentElement.HasChildNodes)
            {
                isCreatorValid = pyramidDoc.DocumentElement.ChildNodes.Count == 1 &&
                                 pyramidDoc.DocumentElement.FirstChild.Name == "uczestnik";
            }
            return isNameValid && isCreatorValid;
        }

        bool SimpleValidateTransactionsXml(XmlDocument transactionsDoc)
        {
            bool isNameValid = transactionsDoc.DocumentElement.Name == "przelewy";
            bool hasValidTransaction = true;
            if (transactionsDoc.DocumentElement.HasChildNodes)
            {
                XmlNode tmp = transactionsDoc.DocumentElement.FirstChild;
                hasValidTransaction = tmp.Attributes.Count == 2 && !string.IsNullOrEmpty(tmp.Attributes["od"].Value);
            }

            return isNameValid && hasValidTransaction;
        }

        Dictionary<string, MemberModel> GeneratePyramidFromXml(XmlDocument pyramidDoc)
        {
            Dictionary<string, MemberModel> output = new Dictionary<string, MemberModel>();
            //Creator
            if (!pyramidDoc.DocumentElement.HasChildNodes)
            {
                return output;
            }
            string creatorId = pyramidDoc.DocumentElement.FirstChild.Attributes["id"].Value;
            output.Add(creatorId, CreatePyramidNode(null, creatorId, 0));
            AddChildrenToParent(output[creatorId], pyramidDoc.DocumentElement.FirstChild.ChildNodes, 1, output);
            CountMembersWithoutSubordinates(output);
            return output;
        }

        MemberModel CreatePyramidNode(MemberModel parent, string id, int level)
        {
            return new MemberModel() { Id = id, Level = level, Subordinates = new List<MemberModel>(), Superior = parent };
        }

        void AddChildrenToParent(MemberModel parent, XmlNodeList childList, int level, Dictionary<string, MemberModel> pyramid)
        {
            if (childList == null)
            {
                return;
            }

            for (int i = 0; i < childList.Count; i++)
            {
                string id = childList.Item(i).Attributes["id"].Value;
                MemberModel newMember = CreatePyramidNode(parent, id, level);
                parent.Subordinates.Add(newMember);
                pyramid.Add(id, newMember);
            }

            for (int i = 0; i < childList.Count; i++)
            {
                AddChildrenToParent(pyramid[childList.Item(i).Attributes["id"].Value], childList.Item(i).ChildNodes, level + 1,pyramid);
            }
        }

        void CountMembersWithoutSubordinates(Dictionary<string, MemberModel> pyramidDictionary)
        {
            foreach (var member in pyramidDictionary.Values)
            {
                if (member.Subordinates.Count == 0)
                {
                    IncreaseSws(member.Superior);
                }
            }
        }

        void IncreaseSws(MemberModel member)
        {
            if (member != null)
            {
                member.SubordinatesWithoutSubordinates += 1;
                IncreaseSws(member.Superior);
            }
        }

        List<TransactionModel> LoadTransactions(XmlDocument transactionsDoc)
        {
            XmlNodeList list = transactionsDoc.DocumentElement.ChildNodes;
            List<TransactionModel> output = new List<TransactionModel>();
            if (list == null)
            {
                return output;
            }
            for (int i = 0; i < list.Count; i++)
            {
                output.Add(new TransactionModel()
                {
                    FromId = list.Item(i).Attributes["od"].Value,
                    Amount = Int32.Parse(list.Item(i).Attributes["kwota"].Value)
                });
            }

            return output;
        }

        public void MakeTransactions()
        {
            if (!_pyramidReady || !_transactionsReady || _pyramid.Count==0)
            {
                return;
            }

            foreach (var transaction in _transactions)
            {

                try
                {
                    MakeTransaction(_pyramid[transaction.FromId], transaction.Amount);
                }
                catch (KeyNotFoundException e)
                {
                    continue;
                }                
            }
        }
        void MakeTransaction(MemberModel fromMember, int amount)
        {
            List<MemberModel> superiors = new List<MemberModel>();
            ListSuperiors(fromMember.Superior, superiors);
            if (fromMember.Superior == null)
            {
                fromMember.Money += amount;
                return;
            }
            superiors.Reverse();
            foreach (var superior in superiors)
            {
                int share = (int)amount / 2;
                superior.Money += share;
                amount -= share;
            }
            superiors[superiors.Count - 1].Money += amount;

        }

        void ListSuperiors(MemberModel superior, List<MemberModel> superiorsList)
        {
            if (superior != null)
            {
                superiorsList.Add(superior);
                ListSuperiors(superior.Superior, superiorsList);
            }
        }

        public void LogPyramid()
        {
            if (!_pyramidReady)
            {
                return;
            }

            List<int> ids = _pyramid.Keys.ToList().ConvertAll(Int32.Parse);
            ids.Sort();
            foreach (var id in ids)
            {
                var member = _pyramid[id.ToString()];
                Console.WriteLine($"{member.Id} {member.Level} {member.SubordinatesWithoutSubordinates} {member.Money}");
            }
        }
            
    }
   
}
